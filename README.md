# Bash, Git, Bitbucket Intro (Markdown) 

## Bash
Where am I? - ```$ pwd```

Where can I go? - ```$ ls```

Changing location - ```$ cd <Directory>```

Making directories - ```$ mkdir <Directory>```

Playing god!/Making files - ```$ touch <File>```

Remove file - ```$ rm <File>```

Remove directory - ```$ rm rf <Directory>```

Clear screen - ```$ clear```

Represents where I am - .

Represents whats above - ..

See manual information on command - ```$ man command```

Print text - ```echo <string>```

Print what's in file - ```$ cat<file>```



## Git
Start git repo - ```git init```

Show new or modified files - ```git status```

Add changes to stage to be commited - ```git add <File> || .```

Commits added changes - ```git commit -m "message"```

Push changes to the origin - ```git push```

git merge

git rebase 

Establishes remote repo that will be used - ```git remote add origin <link to remote repo>```

States what has been set as the remote repo  - ```git remote --v```

Creates connection between local remote repo - ```git push -u origin master```

## Markdowns
Header - #

Header 2 - ##

Header 3 - ###

Code - ``` (Language)

**Bold**

<u>Underlined</u>

*Italics*
